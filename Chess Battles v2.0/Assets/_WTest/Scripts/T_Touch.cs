﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class T_Touch : MonoBehaviour
{
    public Text[] txts;
    private int count;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        count = Input.touchCount;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                Touch t = Input.GetTouch(i);
                 Vector3 v = new Vector3(t.position.x, t.position.y, 0);
                 txts[i].transform.parent.position = v;
                 txts[i].text = t.fingerId.ToString();
            }
        }

    }
}
