﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Đội quân nào Angel, Demon
/// </summary>
[System.Serializable]
public enum AA_ENArmyType
{
    Angel = 1,
    Demon = 2
}
