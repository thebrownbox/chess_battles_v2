﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System;
using System.Collections.Generic;

public class AA_FBHelper
{
    private static AA_FBHelper _instance;
    public static AA_FBHelper Instance
    {
        get
        {
            if (_instance == null)
                _instance = new AA_FBHelper();
            return _instance;
        }
    }

    #region Properties, fields,..
    List<string> perms = new List<string>() { "public_profile", "email", "user_friends" };

    #endregion

    #region Events
    public event Action OnInitialized;
    public event Action OnLoggedIn;
    #endregion

    private AA_FBHelper() { }

    #region Public methods
    public void Login()
    {
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }
    public void Init()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
            if (OnInitialized != null)
                OnInitialized();
        }
    }

    #endregion

    #region Private methods
    private void LoadUserInfo()
    {
        Debug.Log("LoadUserInfo: ...");
        FB.API("me/?fields=first_name,last_name,name,picture,email", HttpMethod.GET, LoadDataDone);
    }

    private void LoadDataDone(IGraphResult result)
    {
        if (result != null && result.Error == null)
        {
            Debug.Log("LoadDataDone: " + result.RawResult);
            FB_UserInfo.Current = JsonUtility.FromJson<FB_UserInfo>(result.RawResult);
            PlayerPrefs.SetString("currentId", FB_UserInfo.Current.id);

            AA_UserData.Current = new AA_UserData();
            AA_UserData.Current.fbInfo = FB_UserInfo.Current;
            AA_UserData.Current.isLoggedIn = true;
            AA_UserData.SaveData();
        }
        else
        {
            Debug.Log("LoadDataDone: " + result.RawResult);
        }
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("AuthCallback: ...");

            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;

            LoadUserInfo();

            if (OnLoggedIn != null)
                OnLoggedIn();
        }
        else
        {
            Debug.Log("AuthCallback: Failed!");
        }
    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
            if (OnInitialized != null)
                OnInitialized();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    #endregion
}
[Serializable]
public class FB_Data
{
    public bool is_silhouette;
    public string url;
}
[Serializable]
public class FB_Picture
{
    public FB_Data data;
}
[Serializable]
public class FB_UserInfo
{
    public FB_UserInfo()
    {
        Current = this;
    }

    public string first_name = "";
    public string email = "";
    public string last_name = "";
    public string name = "";
    public FB_Picture picture;
    public string id = "";

    public static FB_UserInfo Current { get; set; }
}