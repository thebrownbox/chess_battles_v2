﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Phân chia các ô cờ có thể sắp xếp,
/// Chỉ master mới được xếp quân cào vào các vị trí tương ứng.
/// </summary>
[Serializable]
public enum AA_ENCellType {
    MASTER = 1,
    CLIENT = 0,
    NONE = 2
}
