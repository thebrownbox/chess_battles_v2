﻿using System;
using System.Collections.Generic;

/// <summary>
/// Lớp này sử dụng Resouces Manager load hết thông tin có trong resource ra.
/// Ở ngoài thì sử dụng thông qua class này chứ không sử dụng thông qua resouce nữa.
/// </summary>
public class AA_GameData
{
    public static AA_GameData _instance = null;
    public static AA_GameData Instance
    {
        get
        {
            if (_instance == null)
                _instance = new AA_GameData();
            return _instance;
        }
    }

    /// <summary>
    /// Biến trạng thái check xem đã load data thành công chưa.
    /// Nếu load rồi thì không load lại nữa.
    /// Đề phòng trường hợp load lại nhiều lần.
    /// </summary>
    private bool isLoaded = false;

    /// <summary>
    /// Các thông tin này đều được lưu trữ trong file, sẽ load vào lúc lần đầu vào game
    /// </summary>
    #region Các thông tin được lưu vào file

    /// <summary>
    /// Thông tin map data
    /// </summary>
    public Dictionary<AA_ENZone, AA_MapData> MapsData { get; private set; }

    /// <summary>
    /// Hình ảnh sprite của các quân cờ
    /// </summary>
    public Dictionary<AA_ENArmyType, Dictionary<AA_ENPieceType, UnityEngine.Sprite>> PieceSprites { get; private set; }
    #endregion
    private AA_GameData()
    {
        UnityEngine.Debug.Log("AA_GameData()");
        this.MapsData = new Dictionary<AA_ENZone, AA_MapData>();
        this.PieceSprites = new Dictionary<AA_ENArmyType, Dictionary<AA_ENPieceType, UnityEngine.Sprite>>();
    }

    public void LoadData()
    {
        try
        {
            UnityEngine.Debug.Log("LoadData()");
            if (!isLoaded)
            {
                UnityEngine.Debug.Log("Load data in game data is begin!");

                Array zoneArr = Enum.GetValues(typeof(AA_ENZone));
                foreach (AA_ENZone zone in zoneArr)
                    this.MapsData.Add(zone, GetMapData(zone));

                LoadPieceSprites();

                isLoaded = true;
                UnityEngine.Debug.Log("Load data in game data is done!");
            }
        }
        catch (Exception ex)
        {
            UnityEngine.Debug.Log("LoadData: " + ex.Message);
        }
    }

    public UnityEngine.Sprite GetPieceSprite(AA_ENArmyType army, AA_ENPieceType piece)
    {
        if (this.PieceSprites[army].ContainsKey(piece))
            return PieceSprites[army][piece];
        return null;
    }

    #region Các phương thức để lấy các thông tin trên ra

    /// <summary>
    /// Lấy thông tin về giới hạn các quân cờ trên bàn cờ
    /// </summary>
    private AA_PieceLimit GetPieceLimit()
    {
        string text = (AA_ResourceMng.Instance.Get("Data/limit") as UnityEngine.TextAsset).text;
        AA_PieceLimit limitData = UnityEngine.JsonUtility.FromJson<AA_PieceLimit>(text);
        return limitData;
    }

    /// <summary>
    /// Load thông tin map từ file
    /// </summary>
    private AA_MapData GetMapData(AA_ENZone zone)
    {
        Object textObject = AA_ResourceMng.Instance.Get("Data/" + zone.ToString());
        UnityEngine.TextAsset jsonFile = (UnityEngine.TextAsset)(textObject);
        AA_MapData data = UnityEngine.JsonUtility.FromJson<AA_MapData>(jsonFile.text);
        return data;
    }


    /// <summary>
    /// Lấy các icon đại diện cho các quân thuộc đối quân đó.
    /// Icon dùng cho việc select quân trong bàn cờ, và hiện vào giới thiệu của nó.
    /// </summary>
    private void LoadPieceSprites()
    {
        Object[] all = AA_ResourceMng.Instance.GetAll("Sprites/IconSpriteSheet");
        Array armyArr = Enum.GetValues(typeof(AA_ENArmyType));
        foreach (AA_ENArmyType army in armyArr)
            PieceSprites.Add(army, new Dictionary<AA_ENPieceType, UnityEngine.Sprite>());

        ///Đoạn này tên phải đặt đúng cấu trúc ntn thì mới có hiệu quả
        foreach (var obj in all)
        {
            UnityEngine.Sprite sprite = obj as UnityEngine.Sprite;
            if (sprite != null)
            {
                //Lấy ra tên của army và pice
                string[] t = sprite.name.Split('_');
                AA_ENArmyType army = (AA_ENArmyType)Enum.Parse(typeof(AA_ENArmyType), t[0]);
                AA_ENPieceType piece = (AA_ENPieceType)Enum.Parse(typeof(AA_ENPieceType), t[1]);
                this.PieceSprites[army].Add(piece, sprite);
            }
        }
    }

    #endregion
}
