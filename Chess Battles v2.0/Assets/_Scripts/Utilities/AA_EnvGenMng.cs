﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// Generate current envirorment to json into text file
/// </summary>
public class AA_EnvGenMng : MonoBehaviour
{
    public AA_ENZone zone;
#if UNITY_EDITOR
    [ContextMenu("GenerateFile")]
    void GenerateFile()
    {
        string path = GetPathToSave(zone);
        Debug.Log(path);

        AA_MapData info = new AA_MapData();

        //Get cells
        info.cells = new List<AA_CellInfo>();
        foreach (Transform item in transform)
        {
            if (item.name.Equals("cam_1"))
            {
                info.cam_1.pos = item.position;
                info.cam_1.rot = item.rotation;
                
            }
            else if (item.name.Equals("cam_2"))
            {
                info.cam_2.pos = item.position;
                info.cam_2.rot = item.rotation;
            }
            else
            {
                info.cells.Add(item.GetComponent<AA_BaseCell>().GetInfo());
            }
        }
        Debug.Log("count: " + info.cells.Count);

        string content = JsonUtility.ToJson(info);
        File.WriteAllText(path, content);
    }
#endif

    public static string GetPathToSave(AA_ENZone zone)
    {
        return Application.dataPath + "/Resources/Data/" + zone.ToString() + ".txt";
    }
}
