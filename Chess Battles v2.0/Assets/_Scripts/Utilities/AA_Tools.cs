﻿using UnityEngine;
using System.Collections;
using System.IO;

public class AA_Tools : MonoBehaviour
{

    [ContextMenu("Test")]
    public void Test()
    {
        if (1 == 1)
        {

        }
        //NHiệm vụ: chuyển vị trí đang có bit 1 tại a thành bit 0 tại b
        //Sao cho b = 0001 = 1
        int a = 0x0010;
        int b = 0x0011;

        //Giống nhau thì = 0, khác nhau thì  = 1
        Debug.Log(1 << 0);
        Debug.Log(1 << 1);
    }


    public static AA_BoardDataForTranfer GetTestAnemies()
    {
        AA_BoardDataForTranfer data = new AA_BoardDataForTranfer();
        data.pieces.Add(new AA_PieceData() { pieceType = AA_ENPieceType.Hau, location = new AA_Location2D(1, 7), isMaster = false, armyType = AA_ENArmyType.Demon });
        data.pieces.Add(new AA_PieceData() { pieceType = AA_ENPieceType.Ma, location = new AA_Location2D(3, 7), isMaster = false, armyType = AA_ENArmyType.Demon });
        data.pieces.Add(new AA_PieceData() { pieceType = AA_ENPieceType.Tinh, location = new AA_Location2D(4, 7), isMaster = false, armyType = AA_ENArmyType.Demon });
        data.pieces.Add(new AA_PieceData() { pieceType = AA_ENPieceType.Tot, location = new AA_Location2D(7, 7), isMaster = false, armyType = AA_ENArmyType.Demon });
        return data;
    }


}
