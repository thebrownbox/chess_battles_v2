﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class AA_MoveDataForTranfer
{
    [SerializeField]
    public List<AA_MoveData> listMove = new List<AA_MoveData>();

    /// <summary>
    /// Thêm 1 nước đi 
    /// </summary>
    public void Add(AA_MoveData move)
    {
        this.listMove.Add(move);
    }

    /// <summary>
    /// Xóa các nước đi đang lưu
    /// </summary>
    public void Reset()
    {
        listMove.Clear();
    }
}

[System.Serializable]
public class AA_MoveData
{
    [SerializeField]
    public AA_Location2D from;
    [SerializeField]
    public AA_Location2D to;
}
