﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Thông tin bàn cờ dùng để truyền đi
/// </summary>
[System.Serializable]
public class AA_BoardDataForTranfer  
{
    [SerializeField]
    public List<AA_PieceData> pieces = new List<AA_PieceData>();
}

[System.Serializable]
public class AA_PieceData
{
    [SerializeField]
    public AA_ENPieceType pieceType;

    [SerializeField]
    public AA_ENArmyType armyType;
    
    [SerializeField]
    public AA_Location2D location;

    [SerializeField]
    public bool isMaster;
}
