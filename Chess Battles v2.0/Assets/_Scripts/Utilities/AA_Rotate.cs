﻿using UnityEngine;
using System.Collections;

public class AA_Rotate : MonoBehaviour {

    public float velocRotate = 1;
    public bool isRotate = true;
    public Vector3 dir = Vector3.up;
	
	// Update is called once per frame
	void Update () {
        if (isRotate)
        {
            this.transform.Rotate(dir, Space.Self);
        }
	}
}
