﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Danh sách giới hạn số lượng các quân cờ
/// </summary>
[System.Serializable]
public class AA_PieceLimit
{
    public Dictionary<AA_ENPieceType, int> numbers = new Dictionary<AA_ENPieceType, int>();
    public AA_PieceLimit()
    {
        numbers.Add(AA_ENPieceType.Hau, 1);
        numbers.Add(AA_ENPieceType.Ma, 1);
        numbers.Add(AA_ENPieceType.Phao, 1);
        numbers.Add(AA_ENPieceType.Tinh, 1);
        numbers.Add(AA_ENPieceType.Tot, 1);
        numbers.Add(AA_ENPieceType.Vua, 1);
        numbers.Add(AA_ENPieceType.Xe, 1);
    }
}

