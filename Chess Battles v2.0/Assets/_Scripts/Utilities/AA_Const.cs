﻿using UnityEngine;
using System.Collections;
/// <summary>
/// This class contains all the path/link/url that you need in your project.
/// Dont use string, use MyPath.xxx
/// </summary>
public class AA_MyPath
{

    #region Scene's name
    public const string SceneStart = "StartScene";
    public const string SceneTest = "TestScene";
    public const string SceneSelectArmy = "SelectArmyScene";
    public const string SceneSelectBattlefield = "SelectBattlefieldScene";
    public const string SceneMain = "MainScene";
    #endregion


    #region Resource path
    public const string Path_Cell = "_Prefaps/Models/Cell";
    #endregion

    #region Facebook
    public const string FBURL = "123";
    #endregion


    
}

public class RPC_Methods
{
    public const string LoadOtherInfo = "LoadOtherInfo";
}
