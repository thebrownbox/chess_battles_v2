﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Loại quân cờ Vua, Hậu, Tịnh, Mã, Xe, Pháo, Tốt
/// </summary>
public enum AA_ENPieceType 
{
    Vua = 0,
    Hau = 1,
    Phao = 2,
    Xe = 3,
    Tinh = 4,
    Ma = 5,
    Tot = 6
}
