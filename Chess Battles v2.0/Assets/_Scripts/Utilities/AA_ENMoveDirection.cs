﻿using UnityEngine;
using System.Collections;

public enum AA_ENMoveDirection
{
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    UPPER_LEFT,
    UPPER_RIGHT,
    LOWER_LEFT,
    LOWER_RIGHT
}
