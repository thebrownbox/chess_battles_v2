﻿using UnityEngine;
using System.Collections;

public enum AA_ENGameState {
    /// <summary>
    /// Trạng thái đang chờ đợi đối thủ
    /// </summary>
    IS_WAITING = 0x0001,

    /// <summary>
    /// Trạng thái bình thường, trước cuộc chơi
    /// </summary>
    NEW = 0x0002,

    /// <summary>
    /// Trạng thái đang chọn quân để nhét vào bàn cờ
    /// </summary>
    IS_SELECTING = 0x0004,

    /// <summary>
    /// Trạng thái đang chọn quân cờ để đặt
    /// </summary>
    IS_PUTTING = 0x0008,
    
    /// <summary>
    /// Đã sắp xếp quân cờ xong đang đợi người còn lại sẵn sàng là có thể bắt đầu
    /// </summary>
    IS_READY = 0x0010,

    /// <summary>
    /// Trạng thái bắt đầu cuộc chơi
    /// </summary>
	IS_STARTED = 0x0020,

    /// <summary>
    /// Lượt đi của bạn
    /// </summary>
    INGAME_IS_MY_TURN = 0x0040,

    /// <summary>
    /// Đang ở trạng thái tung xúc xắc
    /// </summary>
    IS_ROLLING_DICE = 0x0080,

    /// <summary>
    /// Đang playing animation
    /// </summary>
    IS_PLAYING_ANIMATION = 0x0100
}
