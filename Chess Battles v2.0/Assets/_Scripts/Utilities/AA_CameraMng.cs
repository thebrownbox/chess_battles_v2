﻿using UnityEngine;
using System.Collections;

/*
 Chú ý là nó có 2 chiều, chiều người chơi và chiều đối thủ là khác nhau.
 */
public class AA_CameraMng : MonoBehaviour
{
    private Camera current;
    // The rate of change of the field of view in perspective mode.
    public float perspectiveZoomSpeed = 0.5f;

    // The rate of change of the orthographic size in orthographic mode.
    public float orthoZoomSpeed = 0.5f;

    void Awake()
    {
        if (Camera.main != null)
            current = Camera.main;
        else if (Camera.current != null)
            current = Camera.current;
        else 
            current = GetComponent<Camera>();
    }
    Vector3 p1 = Vector3.zero, p2, d = Vector3.zero;
    void Update()
    {
#if UNITY_EDITOR
        
        if(Input.GetMouseButtonDown(0))
        {
            p1 = Input.mousePosition;
        }

        if(Input.GetMouseButton(0))
        {
            p2 = Input.mousePosition;
            d = p2 - p1;
            p1 = p2;
            Vector3 newPos = current.transform.position + new Vector3(d.x * Time.deltaTime, 0, d.y * Time.deltaTime);
            current.transform.position = Vector3.Lerp(current.transform.position, newPos, Time.deltaTime * 5);
        
        }
#endif
        if (Input.touchCount == 1)
        {
            Touch t = Input.GetTouch(0);
            Vector3 newPos = current.transform.position + new Vector3(t.deltaPosition.x * Time.deltaTime, 0, t.deltaPosition.y * Time.deltaTime);
            current.transform.position = Vector3.Lerp(current.transform.position, newPos, Time.deltaTime * 5);
        }

        // If there are two touches on the device...
        else if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;
            Vector2 touch2PrevPos = touch2.position - touch2.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevDelta = (touch1PrevPos - touch2PrevPos).magnitude;
            float currentDelta = (touch1.position - touch2.position).magnitude;

            // Find the difference in the distances between each frame.
            float delta = prevDelta - currentDelta;
            float newValue;
            // If the camera is orthographic...
            if (current.orthographic)
            {
                // ... change the orthographic size based on the change in distance between the touches.
                newValue = current.orthographicSize + delta * orthoZoomSpeed;

                // Make sure the orthographic size never drops below zero.
                newValue = Mathf.Max(newValue, 0.1f);

                //smooth
                current.orthographicSize = Mathf.Lerp(current.orthographicSize, newValue, Time.deltaTime * 5);
            }
            else
            {
                // Otherwise change the field of view based on the change in distance between the touches.
                newValue = current.fieldOfView + delta * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                newValue = Mathf.Clamp(newValue, 0.1f, 179.9f);

                //smooth
                current.fieldOfView = Mathf.Lerp(current.fieldOfView, newValue, Time.deltaTime * 5);
            }
        }
    }
}
