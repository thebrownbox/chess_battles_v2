﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public struct AA_Location2D
{
    public int X;
    public int Y;
    public override string ToString()
    {
        return "[" + X + "," + Y + "]";
    }
    public AA_Location2D(int x, int y)
    {
        this.X = x;
        this.Y = y;
    }
}
