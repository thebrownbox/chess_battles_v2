﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class AA_BaseCell : MonoBehaviour
{
    public static float CELL_SIZE { get { return 1.2f; } }

    [SerializeField]
    private GameObject effectSelected;
    [SerializeField]
    private GameObject effectTargeted;

    private AA_Location2D _location;

    /// <summary>
    /// Loại ô: 
    /// MASTER: ô thuộc phía master
    /// CLIENT: ô ở phía client
    /// NONE: ô ở giữa, không thuộc phía nào cả
    /// </summary>
    public AA_ENCellType type;
    public AA_ENCellState State { get; private set; }

    //Quân cờ đang ở vị trí này, ko có thì sẽ null
    public AA_BasePiece currentPiece = null;


    /// <summary>
    /// Lấy ra location hiện tại
    /// </summary>
    public AA_Location2D GetLocation()
    {
        return _location;
    }

    public void SetCellState(AA_ENCellState state)
    {
        if (this.State != state)
        {
            this.State = state;

            switch (this.State)
            {
                case AA_ENCellState.NORMAL:
                    effectSelected.SetActive(false);
                    effectTargeted.SetActive(false);
                    break;
                case AA_ENCellState.SELECTED:
                    effectSelected.SetActive(true);
                    effectTargeted.SetActive(false);
                    //effectTargeted.
                    break;
                case AA_ENCellState.TARGETED:
                    effectSelected.SetActive(false);
                    effectTargeted.SetActive(true);
                    break;
                default:
                    break;
            }
        }
    }



    /// <summary>
    /// Get location from position
    /// </summary>
    public static AA_Location2D GetLocation(Transform t)
    {
        AA_Location2D location;
        location.X = (int)Mathf.Round(t.position.x / CELL_SIZE);
        location.Y = (int)Mathf.Round(t.position.z / CELL_SIZE);
        return location;
    }

    /// <summary>
    /// Get Position from location
    /// </summary>
    public static Vector3 GetPosition(AA_Location2D l)
    {
        Vector3 p = new Vector3();
        p.x = CELL_SIZE * l.X;
        p.z = CELL_SIZE * l.Y;
        p.y = 0;
        return p;
    }

    /// <summary>
    /// Thêm 1 thuộc tính mới cần sửa vòa trong này nữa
    /// </summary>
    /// <returns></returns>
    public AA_CellInfo GetInfo()
    {
        AA_CellInfo info = new AA_CellInfo();
        info.loc = GetLocation(this.transform);
        info.type = type;
        return info;
    }

    /// <summary>
    /// Set thông tin khi load từ map vào
    /// </summary>
    public void SetInfo(AA_CellInfo info)
    {
        this.transform.position = GetPosition(info.loc);
        this._location = info.loc;
        this.type = info.type;
    }


    public bool IsMyCell(bool isMaster)
    {
        if (isMaster)
            return type == AA_ENCellType.MASTER;
        else return type == AA_ENCellType.CLIENT;
    }

}
