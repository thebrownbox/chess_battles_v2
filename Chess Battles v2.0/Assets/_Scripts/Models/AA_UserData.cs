﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

[System.Serializable]
public class AA_UserData
{
    [SerializeField]
    public FB_UserInfo fbInfo;

    public List<AA_ArmyData> armies;

    public bool isLoggedIn;
    public string Name { get { return fbInfo.name; } }
    public string Id { get { return fbInfo.id; } }
    public string ImageUrl { get { return "https://graph.facebook.com/" + fbInfo.id + "/picture?type=normal"; } }

    public static AA_UserData Current;
    public static bool SaveData()
    {
        try
        {
            string currentId = PlayerPrefs.GetString("currentId", null);
            if (currentId == null)
                return false;
            string data = JsonUtility.ToJson(Current, true);

            PlayerPrefs.SetString("user_" + currentId, data);

            Debug.Log("SaveData: " + data);
            return true;
        }
        catch (System.Exception e)
        {
            Debug.Log("SaveData: " + e.Message);
            return false;
        }
    }

    public static bool LoadData()
    {
        try
        {
            string currentId = PlayerPrefs.GetString("currentId", null);
            if (currentId == null)
                return false;
            string data = "";

            data = PlayerPrefs.GetString("user_" + currentId);

            Current = JsonUtility.FromJson<AA_UserData>(data);
            Debug.Log("LoadData: " + data);
            if (Current == null)
                return false;
            FB_UserInfo.Current = Current.fbInfo;
            if (Current.armies.Count == 0)
            {
                Current.armies.Add(new AA_ArmyData() { type = AA_ENArmyType.Angel });
                Current.armies.Add(new AA_ArmyData() { type = AA_ENArmyType.Demon });
            }
            return true;
        }
        catch (System.Exception e)
        {
            Debug.Log("LoadData: " + e.Message);
            return false;
        }
    }
}

[System.Serializable]
public class AA_ArmyData
{
    [SerializeField]
    public AA_ENArmyType type;
    public AA_ArmyData()
    {
        for (int i = 0; i < 16; i++)
            isUnlocked.Add(false);
    }

    public List<bool> isUnlocked = new List<bool>();

    public void Unlock(int index)
    {
        if (index < 16)
            isUnlocked[index] = true;
    }
}
