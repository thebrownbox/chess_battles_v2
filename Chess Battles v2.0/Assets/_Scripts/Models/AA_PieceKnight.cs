﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AA_PieceKnight : AA_BasePiece
{
    public override List<AA_BaseCell> GetMoveableCell(AA_ChessBoard board, int diceValue)
    {
        List<AA_Location2D> locations = new List<AA_Location2D>();
        if (diceValue >= 3 && !isMoved)
        {
            locations.Add(new AA_Location2D() { X = CurrentLocation.X + 2, Y = CurrentLocation.Y + 1 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X + 2, Y = CurrentLocation.Y - 1 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X - 2, Y = CurrentLocation.Y + 1 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X - 2, Y = CurrentLocation.Y - 1 });

            locations.Add(new AA_Location2D() { X = CurrentLocation.X + 1, Y = CurrentLocation.Y + 2 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X - 1, Y = CurrentLocation.Y + 2 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X + 1, Y = CurrentLocation.Y - 2 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X - 1, Y = CurrentLocation.Y - 2 });
        }
        return GetMoveableCellFromLocation(locations, board, diceValue);
    }
}
