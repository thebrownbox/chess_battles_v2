﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Quân này hành xử giống hệt quân pháo trong cờ tướng
/// </summary>
public class AA_PieceCannon : AA_BasePiece
{
    public override List<AA_BaseCell> GetMoveableCell(AA_ChessBoard board, int diceValue)
    {
        List<AA_BaseCell> list = new List<AA_BaseCell>();
        if (!isMoved)
        {
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.BACKWARD);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.FORWARD);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.LEFT);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.RIGHT);
        }

        return list;
    }
}
