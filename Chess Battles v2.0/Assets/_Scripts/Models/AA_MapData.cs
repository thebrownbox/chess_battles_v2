﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// Cấu trúc của 1 mapdata, những thông tin của 1 map được lưu ở trong đây
/// </summary>
[Serializable]
public class AA_MapData
{
    public List<AA_CellInfo> cells;
    public AA_CamInfo cam_1;
    public AA_CamInfo cam_2;
}


/// <summary>
/// Thông tin về vị trí của các ô cờ
/// </summary>
[Serializable]
public class AA_CellInfo  {
    public AA_Location2D loc;
    public AA_ENCellType type;
}

/// <summary>
/// Thông tin về vị trí của camera
/// </summary>
[Serializable]
public struct AA_CamInfo
{
    public Vector3 pos;
    public Quaternion rot;
}

