﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AA_PieceBishop : AA_BasePiece
{

    public override List<AA_BaseCell> GetMoveableCell(AA_ChessBoard board, int diceValue)
    {
        List<AA_BaseCell> list = new List<AA_BaseCell>();
        if (!isMoved)
        {
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.UPPER_LEFT);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.UPPER_RIGHT);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.LOWER_RIGHT);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.LOWER_LEFT);
        }
        return list;
    }

}
