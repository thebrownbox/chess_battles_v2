﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AA_PieceKing : AA_BasePiece
{
    /// <summary>
    /// Quân vua:
    /// - Đi được tứ phía
    /// </summary>
    public override List<AA_BaseCell> GetMoveableCell(AA_ChessBoard board, int diceValue)
    {
        List<AA_Location2D> locations = new List<AA_Location2D>();
        if (diceValue > 0 && !isMoved)
        {
            locations.Add(new AA_Location2D() { X = CurrentLocation.X + 1, Y = CurrentLocation.Y });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X - 1, Y = CurrentLocation.Y });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X, Y = CurrentLocation.Y + 1 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X + 1, Y = CurrentLocation.Y + 1 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X - 1, Y = CurrentLocation.Y + 1 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X, Y = CurrentLocation.Y - 1 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X + 1, Y = CurrentLocation.Y - 1 });
            locations.Add(new AA_Location2D() { X = CurrentLocation.X - 1, Y = CurrentLocation.Y - 1 });
        }
        return GetMoveableCellFromLocation(locations, board, diceValue);
    }
}
