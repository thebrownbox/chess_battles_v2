﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public abstract class AA_BasePiece : MonoBehaviour
{

    protected bool isMoved = false;

    /// <summary>
    /// Kiểm tra xem có phải làm lần di chuyển đầu tiên không.
    /// Dùng trong trường hợp quân có nước đi đầu tiên đặc biệt.
    /// </summary>
    protected bool isFirstMove = true;

    /// <summary>
    /// Nhiều nước đi phụ thuộc vào hướng, ví dụ như của quân tốt!
    /// Vì đang cố định vị trí của Master và Client trên bàn cờ,
    /// Do đó cần 1 biến tham chiếu để định hướng.
    /// => Nếu là Master thì các thông số để mặc định.
    /// => Nếu là Client thì các thông số cần thay đổi cho phù hợp.
    /// (Ví dụ như góc quay hướng đi)
    /// </summary>
    public bool IsMaster { get; protected set; }

    /// <summary>
    /// Kiểm tra xem quân đó có phải của mình không
    /// => Cách kiểm tra 2 quân có thuộc 2 đội khác nhau không đó là a.IsMaster == b.IsMaster
    /// </summary>
    public bool IsSameSide(bool isMaster)
    {
        return this.IsMaster == isMaster;
    }

    /// <summary>
    /// Xem có cùng đội quân hay không
    /// </summary>
    public bool IsSameSide(AA_BasePiece otherPiece)
    {
        return this.armyType == otherPiece.armyType;
    }

    /// <summary>
    /// Lấy các ô có thể di chuyển
    /// </summary>
    /// <returns></returns>
    public abstract List<AA_BaseCell> GetMoveableCell(AA_ChessBoard board, int diceValue);

    /// <summary>
    /// Loại quân cờ
    /// </summary>
    public AA_ENPieceType pieceType;

    /// <summary>
    /// Quân cờ thuộc đội quân nào
    /// </summary>
    public AA_ENArmyType armyType;

    /// <summary>
    /// Ô cờ mà quân hiện tại đang đứng
    /// </summary>
    public AA_BaseCell CurrenCell { get; private set; }

    public AA_Location2D CurrentLocation { get { return CurrenCell.GetLocation(); } }

    /// <summary>
    /// Chuyển vị trí quân cờ vào vị trí ô cell được chọn
    /// 1. Dịch chuyển vị trí quân cờ
    /// 2. Gán lại tham chiếu ở ô cờ cũ = null
    /// </summary>
    public void InitPosition(AA_BaseCell newCell)
    {
        if (newCell != null)
        {
            this.transform.position = newCell.transform.position;
            //Nếu trước đang ở ô khác thì xóa tham chiếu ở ô cũ đi
            if (CurrenCell != null)
                CurrenCell.currentPiece = null;

            //Thêm tham chiếu ở ô mới vào của bên này cugnx nhở ở bên kia.
            CurrenCell = newCell;
            newCell.currentPiece = this;
        }
        else
        {
            Debug.Log(this.pieceType.ToString() + ": Cell không tồn tại!");
        }
    }

    /// <summary>
    /// Kiểm tra có thể di chuyển được quân cờ này không với lượng dice hiện tại
    /// </summary>
    public bool IsMoveable(int diceValue)
    {

        return true;
    }

    /// <summary>
    /// Thực hiện y như SetPosition, nhưng mà di chuyển nó chậm hơn
    /// Đúng là kiểu move chứ ko phải là set position
    /// 
    /// Chú ý, ở đây xử lý về mặt logic tức thì.
    /// Animation nên thực hiện theo 1 cách khác.
    /// </summary>
    public int MoveTo(AA_ChessBoard board, AA_BaseCell newCell)
    {
        AA_Location2D oldPos = CurrentLocation;

        //Xóa tham chiếu của con đấy trong bàn  cờ ra.
        // && !newCell.currentPiece.IsSameSide(this)
        if (newCell.currentPiece != null)
            board.Remove(newCell.currentPiece);

        //this.transform.position = newCell.transform.position;
        this.transform.DOMove(newCell.transform.position, 0.5f).SetEase(Ease.Linear);

        //Nếu trước đang ở ô khác thì xóa tham chiếu của nó ở ô cũ đi
        if (CurrenCell != null)
            CurrenCell.currentPiece = null;

        //Thêm tham chiếu ở ô mới vào của bên này cugnx nhở ở bên kia.
        CurrenCell = newCell;
        newCell.currentPiece = this;
        isFirstMove = false;

        //Đánh dấu để nó không thể đi được lần sau nữa
        isMoved = true;

        //Trả về số ô di chuyển
        return GetDistance(this.pieceType, oldPos, CurrentLocation);
    }

    public string GetPathInResource()
    {
        return string.Format("Prefaps/{0}_{1}", armyType, pieceType);
    }


    /// <summary>
    /// Một quy tắc mới!
    /// Tất cả các class mà cần thông tin setup thì cần thêm 1 hàm tên là Setup để thiết lập các thông số
    /// ngay lúc đầu.
    /// </summary>
    public void SetData(AA_PieceData pData, AA_ChessBoard board)
    {
        this.pieceType = pData.pieceType;
        this.IsMaster = pData.isMaster;
        this.armyType = pData.armyType;
        InitPosition(board.GetCell(pData.location));
    }


    /// <summary>
    /// Lấy ra cell từ list location có thể move được.
    /// => Hàm này chỉ dùng cho các quân cờ có nước đi đơn giản: Tốt, Vua
    /// </summary>
    public List<AA_BaseCell> GetMoveableCellFromLocation(List<AA_Location2D> locations, AA_ChessBoard board, int dice)
    {
        List<AA_BaseCell> list = new List<AA_BaseCell>();
        foreach (AA_Location2D location in locations)
        {
            if (GetDistance(location) <= dice)
            {
                AA_BaseCell cell = board.GetCell(location);
                if (cell != null)
                {
                    //Đang không có quân nào hoặc là đang có quân địch đứng đó
                    if (cell.currentPiece == null || cell.currentPiece.IsMaster != this.IsMaster)
                    {
                        list.Add(cell);
                    }
                }
            }
        }
        return list;
    }

    protected int GetDistance(AA_Location2D newLocaiton)
    {
        return GetDistance(this.pieceType, this.CurrentLocation, newLocaiton);
    }

    public static int GetDistance(AA_ENPieceType type, AA_Location2D old, AA_Location2D newPos)
    {
        switch (type)
        {
            //Đi ngang dọc
            case AA_ENPieceType.Phao:
            case AA_ENPieceType.Xe:
                return Mathf.Abs(old.X - newPos.X) + Mathf.Abs(old.Y - newPos.Y);

            //Đi chéo ngang dọc
            case AA_ENPieceType.Vua:
            case AA_ENPieceType.Hau:
            case AA_ENPieceType.Tot:
                if (old.X == newPos.X)
                    return Mathf.Abs(old.Y - newPos.Y);
                else return Mathf.Abs(old.X - newPos.X);

            //Đi chéo
            case AA_ENPieceType.Tinh:
                return Mathf.Abs(old.X - newPos.X);

            case AA_ENPieceType.Ma:
                return 3;

            default:
                return 0;
        }
    }

    public void SetData(bool isMaster)
    {
        this.IsMaster = isMaster;
    }

    /// <summary>
    /// Lấy các thông tin cần thiết để truyền đi.
    /// (ko phải là truyền tất cả)
    /// </summary>
    public AA_PieceData GetData()
    {
        AA_PieceData pData = new AA_PieceData();
        pData.pieceType = pieceType;
        pData.location = CurrenCell.GetLocation();
        pData.armyType = armyType;
        pData.isMaster = this.IsMaster;
        return pData;
    }

    /// <summary>
    /// Lấy các nước đi có thể đi được theo hướng.
    /// Có xử lý đặc biệt cho quân pháo.
    /// </summary>
    protected void GetMoveableDir(AA_ChessBoard board, List<AA_BaseCell> list, int diceValue, AA_ENMoveDirection dir)
    {
        for (int i = 1; i <= diceValue; i++)
        {
            #region duyệt
            AA_Location2D loca = new AA_Location2D();
            switch (dir)
            {
                case AA_ENMoveDirection.FORWARD:
                    loca = new AA_Location2D(CurrentLocation.X, CurrentLocation.Y + i);
                    break;
                case AA_ENMoveDirection.BACKWARD:
                    loca = new AA_Location2D(CurrentLocation.X, CurrentLocation.Y - i);
                    break;
                case AA_ENMoveDirection.LEFT:
                    loca = new AA_Location2D(CurrentLocation.X - i, CurrentLocation.Y);
                    break;
                case AA_ENMoveDirection.RIGHT:
                    loca = new AA_Location2D(CurrentLocation.X + i, CurrentLocation.Y);
                    break;
                case AA_ENMoveDirection.UPPER_LEFT:
                    loca = new AA_Location2D(CurrentLocation.X - i, CurrentLocation.Y + i);
                    break;
                case AA_ENMoveDirection.UPPER_RIGHT:
                    loca = new AA_Location2D(CurrentLocation.X + i, CurrentLocation.Y + i);
                    break;
                case AA_ENMoveDirection.LOWER_LEFT:
                    loca = new AA_Location2D(CurrentLocation.X - i, CurrentLocation.Y - i);
                    break;
                case AA_ENMoveDirection.LOWER_RIGHT:
                    loca = new AA_Location2D(CurrentLocation.X + i, CurrentLocation.Y - i);
                    break;
                default:
                    break;
            }

            AA_BaseCell cell = board.GetCell(loca);

            // Không có ô cờ không thể di chuyển tiếp được
            if (cell == null)
                break;

            var p = cell.currentPiece;
            // Nếu là ô cờ trống
            if (p == null)
            {
                list.Add(cell);
            }
            // Nếu trong ô cờ có quân cờ
            else
            {
                // Cờ bên ta
                if (this.IsSameSide(p))
                {
                    // Quân cờ cùng bên không đi được
                    // Nếu là quân pháo thì tìm đi tiếp xem gặp được ko
                    if (pieceType == AA_ENPieceType.Phao)
                    {
                        // Số nước đi còn lại
                        int diceLeft = diceValue - i;
                        TimQuanDeAnChoQuanPhaoTuViTriQuanMinh(loca, board, list, diceLeft, dir);
                    }
                    break;
                }
                // Cờ bên địch
                else
                {
                    // Quân thường thì có thể ăn
                    // Riêng với quân pháo không thể ăn
                    if (pieceType != AA_ENPieceType.Phao)
                        list.Add(cell);
                    break;
                }
            }

            #endregion
        }

    }

    protected  void TimQuanDeAnChoQuanPhaoTuViTriQuanMinh(AA_Location2D startLoc, AA_ChessBoard board, List<AA_BaseCell> list, int diceValue, AA_ENMoveDirection dir)
    {
        for (int i = 1; i <= diceValue; i++)
        {
            #region duyệt
            AA_Location2D loca = new AA_Location2D();
            switch (dir)
            {
                case AA_ENMoveDirection.FORWARD:
                    loca = new AA_Location2D(startLoc.X, startLoc.Y + i);
                    break;
                case AA_ENMoveDirection.BACKWARD:
                    loca = new AA_Location2D(startLoc.X, startLoc.Y - i);
                    break;
                case AA_ENMoveDirection.LEFT:
                    loca = new AA_Location2D(startLoc.X - i, startLoc.Y);
                    break;
                case AA_ENMoveDirection.RIGHT:
                    loca = new AA_Location2D(startLoc.X + i, startLoc.Y);
                    break;
                default:
                    break;
            }

            AA_BaseCell cell = board.GetCell(loca);
            if (cell == null)
                break;

            var p = cell.currentPiece;
            //Tìm đến ô cờ tiếp theo có quân địch.
            if (p != null)
            {
                // địch
                if (this.IsSameSide(p) == false)
                    list.Add(cell);
                break;
            }
            #endregion
        }

    }

    protected virtual void Start()
    {
        if (!IsMaster)
        {
            //Nếu đang là Client mới thêm vào thì sẽ xoay người đúng theo nó.
            this.transform.localRotation = new Quaternion() { eulerAngles = new Vector3(0, 180, 0) };
        }
    }

    void OnEnable()
    {
        AA_GameMng.Current.OnMyTurnStart += Event_OnNewTurnStart;
    }
    void OnDisable()
    {
        AA_GameMng.Current.OnMyTurnStart -= Event_OnNewTurnStart;
    }
    void Event_OnNewTurnStart()
    {
        isMoved = false;
    }


}
