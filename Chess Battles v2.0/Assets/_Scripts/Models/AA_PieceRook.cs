﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AA_PieceRook : AA_BasePiece
{
    /// <summary>
    /// Quân xe đi ngang dọc theo giới hạn
    /// 1. Tiến tới giới hạn của ô
    /// 2. Gặp quân địch hoặc ta đề phải dừng lại
    /// </summary>
    public override List<AA_BaseCell> GetMoveableCell(AA_ChessBoard board, int diceValue)
    {
        List<AA_BaseCell> list = new List<AA_BaseCell>();
        if (!isMoved)
        {
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.BACKWARD);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.FORWARD);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.LEFT);
            GetMoveableDir(board, list, diceValue, AA_ENMoveDirection.RIGHT);
        }
        return list;
    }


}
