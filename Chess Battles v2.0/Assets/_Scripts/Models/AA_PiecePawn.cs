﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AA_PiecePawn : AA_BasePiece
{
    /// <summary>
    /// Nước đi của quân tốt:
    /// 1. Đi được 8 ô xung quanh nó
    /// </summary>
    public override List<AA_BaseCell> GetMoveableCell(AA_ChessBoard board, int diceValue)
    {
        List<AA_Location2D> locations = new List<AA_Location2D>();
        if (diceValue > 0 && !isMoved)
        {
            if (IsMaster)
            {
                locations.Add(new AA_Location2D() { X = CurrentLocation.X + 1, Y = CurrentLocation.Y });
                locations.Add(new AA_Location2D() { X = CurrentLocation.X - 1, Y = CurrentLocation.Y });
                locations.Add(new AA_Location2D() { X = CurrentLocation.X, Y = CurrentLocation.Y + 1 });

                if (diceValue > 1 && isFirstMove)
                    locations.Add(new AA_Location2D() { X = CurrentLocation.X, Y = CurrentLocation.Y + 2 });
            }
            else
            {
                locations.Add(new AA_Location2D() { X = CurrentLocation.X + 1, Y = CurrentLocation.Y });
                locations.Add(new AA_Location2D() { X = CurrentLocation.X - 1, Y = CurrentLocation.Y });
                locations.Add(new AA_Location2D() { X = CurrentLocation.X, Y = CurrentLocation.Y - 1 });

                if (diceValue > 1 && isFirstMove)
                    locations.Add(new AA_Location2D() { X = CurrentLocation.X, Y = CurrentLocation.Y - 2 });
            }
        }

        return GetMoveableCellFromLocation(locations, board, diceValue);
    }
}
