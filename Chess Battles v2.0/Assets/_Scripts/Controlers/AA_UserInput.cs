﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

/// <summary>
/// Module này dùng để bắt được ô người dùng chọn
/// </summary>
public class AA_UserInput : MonoBehaviour
{
    public static AA_UserInput Current;
    public static AA_BaseCell currentCell;

    public LayerMask CellLayerMask = 0;
    public event System.Action<AA_BaseCell> OnSelectedCell;

    public AA_UserInput() { Current = this; }

    //Xử lý xác định xem ô cờ nào đang được chọn
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject() == false)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Debug.DrawRay(ray.origin, ray.direction * 1000);

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 1000000, CellLayerMask.value))
                {
                    AA_BaseCell cell = hit.collider.GetComponent<AA_BaseCell>();
                    if (cell == null)
                        return;
                    currentCell = cell;
                    if (OnSelectedCell != null)
                        OnSelectedCell(currentCell);
                }
            }
        }
    }


}
