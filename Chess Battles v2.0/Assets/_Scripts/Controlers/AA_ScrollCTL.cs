﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class AA_ScrollCTL : MonoBehaviour {
    private ScrollRect _scroll;
    void OnEnable()
    {
        Debug.Log("OnEnable");
        if (_scroll == null)
            _scroll = GetComponent<ScrollRect>();
        _scroll.verticalNormalizedPosition = 1;
    }
}
