﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class AA_SelectNhanVat : MonoBehaviour
{
    public static AA_SelectNhanVat Current;
    public GameObject groupButtons;

    public Canvas panel;
    public GameObject item;
    public Transform group;
    private AA_BasePiece currentSelectedPiece;

    /// <summary>
    /// Lưu giới hạn các quân cờ của lượt chơi
    /// </summary>
    private AA_PieceLimit currentLimit = new AA_PieceLimit();

    /// <summary>
    /// Các button trên menu select
    /// </summary>
    private Dictionary<AA_ENPieceType, Button> buttons = new Dictionary<AA_ENPieceType, Button>();
    private Dictionary<AA_ENPieceType, Text> texts = new Dictionary<AA_ENPieceType, Text>();
    public AA_ENGameState GameState
    {
        get { return AA_GameMng.Current.GameState; }
    }


    #region Các events mà nó cung cấp ra bên ngoài

    /// <summary>
    /// Sự kiện chọn xong nhân vật ở trong menu done
    /// </summary>
    public event System.Action OnSelectedNhanVatDone;

    #endregion

    void Awake()
    {
        Current = this;
    }

    void Start()
    {
        groupButtons.SetActive(!AA_NetworkMng.IsMaster);

        //Load hết danh sách đội quân đã được chọn ra, 
        //Sau đó so sánh với đội quân đã được active
        //Con nào ẩn con nào không ẩn thì lưu vào
        var pieceArr = System.Enum.GetValues(typeof(AA_ENPieceType));
        foreach (AA_ENPieceType piece in pieceArr)
        {
            Sprite sprite = AA_GameData.Instance.GetPieceSprite(AA_UserSelected.SelectedArmy, piece);

            GameObject newItem = Instantiate<GameObject>(item);
            newItem.transform.SetParent(group);
            newItem.transform.localScale = new Vector3(1, 1, 1);
            newItem.SetActive(true);
            buttons.Add(piece, newItem.GetComponent<Button>());
            texts.Add(piece, newItem.transform.GetChild(0).GetComponent<Text>());


            if (sprite != null)
            {
                newItem.GetComponent<Image>().sprite = sprite;
                string imageName = sprite.name;
                buttons[piece].onClick.AddListener(() => { Event_PieceSelected(imageName); });
            }
            else
            {
                currentLimit.numbers[piece] = 0;

                newItem.transform.GetChild(0).GetComponent<Text>().text = currentLimit.numbers[piece].ToString();
            }
            texts[piece].text = currentLimit.numbers[piece].ToString();

            if (currentLimit.numbers[piece] <= 0)
                buttons[piece].interactable = false;
        }

        if (AA_GameMng.IsTestMode)
        {
            groupButtons.SetActive(true);
        }
    }

    /// <summary>
    /// Sự kiện chọn được 1 quân từ list quân
    /// </summary>
    public void Event_PieceSelected(string name)
    {
        //Tìm đến quân theo tên
        string path = "Prefaps/" + name;
        Debug.Log("Path: " + path);
        GameObject pieceGO = GameObject.Instantiate<GameObject>(AA_ResourceMng.Instance.Get(path) as GameObject);
        currentSelectedPiece = pieceGO.GetComponent<AA_BasePiece>();
        currentSelectedPiece.SetData(AA_NetworkMng.IsMaster);


        //Đặt vào ô đang trống
        AA_BaseCell cell = AA_ChessBoard.Current.GetAvaiableCell(AA_UserSelected.SelectedArmy);
        cell.currentPiece = currentSelectedPiece;
        currentSelectedPiece.InitPosition(cell);

        //Tắt panel
        panel.gameObject.SetActive(false);

        //Hiển thị các chỗ có thể đặt vào được
        AA_ChessBoard.Current.ShowSetupable(AA_UserSelected.SelectedArmy);

        //Chuyển trạng thái game
        AA_GameMng.Current.ResetGameState(AA_ENGameState.IS_PUTTING);
    }



    /// <summary>
    /// Chuyển mode sang chế độ chọn nhân vật
    /// - Nếu đang có một nhân vật được chọn thì sẽ hiểu là nó đã được chọn
    /// </summary>
    public void Click_AddNhanVat()
    {
        //Thêm vào game manager
        if (currentSelectedPiece != null)
        {
            CapNhatSoLuong(currentSelectedPiece.pieceType);
            AA_ChessBoard.Current.Add(currentSelectedPiece);
            currentSelectedPiece = null;
        }

        panel.gameObject.SetActive(true);
        AA_GameMng.Current.ResetGameState(AA_ENGameState.IS_SELECTING);
    }

    /// <summary>
    /// Chuẩn bị xong.
    /// - Nếu đang có một nhân vật được chọn thì sẽ hiểu là nó đã được chọn
    /// </summary>
    public void Click_Ready()
    {
        //Thêm quân piece đang được chọn vào game manager
        if (currentSelectedPiece != null)
        {
            CapNhatSoLuong(currentSelectedPiece.pieceType);
            AA_ChessBoard.Current.Add(currentSelectedPiece);
            currentSelectedPiece = null;
        }

        //Kiểm tra xem có ok không
        if (CheckIsValid())
        {
            //Chuyển game state
            AA_GameMng.Current.ResetGameState(AA_ENGameState.IS_READY);

            //ẩn các nút đó đi
            groupButtons.SetActive(false);

            //Bật biến trạng thái ready
            AA_GameMng.Current.IsCurrentReady = true;
            AA_UISceneMain.Current.SetReadyOn(AA_NetworkMng.IsMaster);

            //Gửi thông tin bàn cờ sang bên máy bên kia
            AA_BoardDataForTranfer boardData = AA_ChessBoard.Current.GetCurrentBoardData();
            AA_NetworkMng.Instance.Send_ChessBoardData(boardData);

            //Nếu cả 2 bên đầu Ready rồi thì bắt đầu trận đấu
            if (AA_GameMng.Current.IsReady)
                AA_GameMng.Current.StartTheGame();

            if (OnSelectedNhanVatDone != null)
                OnSelectedNhanVatDone();
        }
        else
        {
            Debug.Log("Chưa ok");
        }
    }

    /// <summary>
    /// Hủy quân đang được chọn
    /// </summary>
    public void Click_Huy()
    {
        if (currentSelectedPiece != null)
        {
            Destroy(currentSelectedPiece.gameObject);
            currentSelectedPiece = null;
        }
        AA_GameMng.Current.ResetGameState(AA_ENGameState.NEW);
    }

    /// <summary>
    /// Cập nhật lại số lượng quân cờ vừa được chọn.
    /// Disable nếu hết.
    /// </summary>
    /// <param name="piece"></param>
    private void CapNhatSoLuong(AA_ENPieceType piece)
    {
        Debug.Log("CapNhatSoLuong");
        if (currentLimit.numbers[piece] > 0)
        {
            currentLimit.numbers[piece]--;
            texts[piece].text = currentLimit.numbers[piece].ToString();
            if (currentLimit.numbers[piece] == 0)
            {
                buttons[piece].interactable = false;
            }
        }

    }

    /// <summary>
    /// Kiểm tra việc xếp quân cờ như thế đã ok chưa.
    /// TRUE: Nếu như tất cả các quân cờ đã được xếp ra.
    /// FALSE: Các trường hợp còn lại.
    /// 
    /// UPDATE: chỉ có 1 điều kiện duy nhất là phải có quân vua vào là được
    /// </summary>
    private bool CheckIsValid()
    {
        if (AA_GameMng.IsTestMode)
            return true;
        
        return currentLimit.numbers[AA_ENPieceType.Vua] == 0;
    }

    #region Events
    void OnEnable()
    {
        AA_UserInput.Current.OnSelectedCell += Event_OnSelectedCell;
        AA_NetworkMng.Instance.OnOtherPlayerJoined += Event_OnOtherPlayerJoined;
    }

    void OnDisable()
    {
        AA_UserInput.Current.OnSelectedCell -= Event_OnSelectedCell;
        AA_NetworkMng.Instance.OnOtherPlayerJoined -= Event_OnOtherPlayerJoined;
    }

    /// <summary>
    /// Sự kiện người dùng click vào 1 cái cell:
    /// 1. Đang ở trạng thái chọn quân.
    /// 2. Ô đấy thuộc về army hiện tại
    /// 3. Ô đấy đang trống
    /// </summary>
    void Event_OnSelectedCell(AA_BaseCell selectedCell)
    {
        if (AA_GameMng.Current.IsGameState(AA_ENGameState.IS_PUTTING)
            && selectedCell.IsMyCell(AA_NetworkMng.IsMaster))
        {
            if (selectedCell.currentPiece == null && currentSelectedPiece != null)
                currentSelectedPiece.InitPosition(selectedCell);
        }
    }

    /// <summary>
    /// Sự kiện một người chơi mới vào
    /// </summary>
    void Event_OnOtherPlayerJoined()
    {
        //Hiển thị các nút chon quân
        groupButtons.SetActive(true);
    }
    #endregion

}
