﻿using UnityEngine;
using System.Collections;

/**
 * Lớp này quản lý việc thực hiện các animation trong trò chơi.
 * - Thực hiện chạy animation lần lượt.
 * - 
 **/
public class AA_AnimationMng : MonoBehaviour
{
    public static AA_AnimationMng Current { get; private set; }
    public event System.Action OnAnimationDone;
    AA_AnimationMng() { Current = this; }
    private AA_ChessBoard chessBoard;
    private AA_MoveDataForTranfer moveData;

    public void GenerateMove(AA_ChessBoard chessBoard, AA_MoveDataForTranfer moveData)
    {
        this.chessBoard = chessBoard;
        this.moveData = moveData;

        //Thực hiện các nước đi
        foreach (AA_MoveData move in moveData.listMove)
        {
            AA_BasePiece currentPiece = chessBoard.GetCell(move.from).currentPiece;
            AA_BaseCell newCell = chessBoard.GetCell(move.to);

            currentPiece.MoveTo(chessBoard, newCell);
        }

        if (OnAnimationDone != null)
            OnAnimationDone();
    }
}