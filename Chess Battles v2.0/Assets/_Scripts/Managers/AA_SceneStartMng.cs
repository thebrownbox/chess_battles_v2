﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Facebook.Unity;
using System.Collections.Generic;

public class AA_SceneStartMng : MonoBehaviour {

    void Awake()
    {
        AA_FBHelper.Instance.Init();
        AA_FBHelper.Instance.OnLoggedIn += Instance_OnLoggedIn;
    }

    void Start()
    {
       
    }


    void Instance_OnLoggedIn()
    {
        
    }

    public void Click_Start()
    {
        SceneManager.LoadScene(AA_MyPath.SceneSelectArmy);
    }
 

    public void Click_Login()
    {
        AA_FBHelper.Instance.Login();
    }

 
}
