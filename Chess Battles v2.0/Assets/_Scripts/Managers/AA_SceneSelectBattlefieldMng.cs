﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AA_SceneSelectBattlefieldMng : MonoBehaviour
{
    #region UI's events
    public void Click_Select(int a)
    {
        AA_UserSelected.SelectedZone = (AA_ENZone)a;

        AA_TransactionMng.Instance.SetStatus("Joining...");
        AA_TransactionMng.Instance.Close();

        if (AA_GameMng.IsTestMode == false)
            AA_NetworkMng.Instance.Join();
        else
            SceneManager.LoadScene(AA_MyPath.SceneMain);
    }
    void Instance_OnJoinedOK()
    {
        SceneManager.LoadScene(AA_MyPath.SceneMain);
    }


    void OnEnable()
    {
        AA_NetworkMng.Instance.OnJoinedOK += Instance_OnJoinedOK;
    }

    void OnDisable()
    {
        AA_NetworkMng.Instance.OnJoinedOK -= Instance_OnJoinedOK;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(AA_MyPath.SceneSelectArmy);
        }
    }
    #endregion
}
