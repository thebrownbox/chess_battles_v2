﻿using UnityEngine;
using System.Collections;
using System;

public class AA_NetworkMng : Photon.MonoBehaviour
{
    private static AA_NetworkMng _instance = null;
    public static AA_NetworkMng Instance { get { return _instance; } }
    public int viewId = 999;
    #region Properties's sate

    public event Action OnJoinedOK;
    public event Action OnLeftOK;
    public event Action OnOtherPlayerJoined;

    public bool IsLoadOtherDataDone { get; set; }

    public bool IsConnected { get; private set; }
    public bool IsJoined { get; private set; }
    public PhotonView photon { get; private set; }
    private PeerState _state;
    private void OnConnectionStateChanged()
    {
        //  AA_TransactionMng.Instance.SetStatus(_state.ToString());
        Debug.Log(_state);
        switch (_state)
        {
            case PeerState.JoinedLobby:
                IsConnected = true;
                break;
            case PeerState.Joined:
                IsJoined = true;
                break;
            case PeerState.Leaving:
                IsJoined = false;
                break;
            case PeerState.Disconnected:
                IsConnected = false;
                break;
            default:
                break;
        }
    }

    #endregion

    void Awake()
    {
        IsLoadOtherDataDone = false;
        if (_instance == null)
        {
            Debug.Log("Khởi tạo Network!");
            _instance = this;
            photon = this.gameObject.AddComponent<PhotonView>();
            photon.viewID = viewId;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Debug.Log("Destroy Network!");
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        if (!AA_GameMng.IsTestMode)
        {
            PhotonNetwork.autoJoinLobby = false;
            PhotonNetwork.ConnectUsingSettings("v1.0");
            _state = PhotonNetwork.connectionStateDetailed;
            PhotonNetwork.OnEventCall += EventCallback;
        }
    }

    void EventCallback(byte eventCode, object content, int senderId)
    {

    }

    void Update()
    {
        if (PhotonNetwork.connectionStateDetailed != _state)
        {
            _state = PhotonNetwork.connectionStateDetailed;
            OnConnectionStateChanged();
        }
    }

    #region Public method for Scene Manger call
    //Không nên thực hiện các công việc liên quan đến mạng ở ngoài này

    public void Join()
    {
        //Kết nối vào Lobby trong
        AA_TransactionMng.Instance.Close();
        if (!AA_GameMng.IsTestMode)
        {
            PhotonNetwork.JoinLobby(new TypedLobby() { Name = AA_UserSelected.SelectedZone.ToString() });
        }
        else
        {
            if (OnJoinedOK != null)
                OnJoinedOK();
        }
    }

    public void Logout()
    {
        if (AA_GameMng.IsTestMode)
        {
            if (OnLeftOK != null)
                OnLeftOK();
        }
        else
        {
            Debug.Log("Log out!");
            PhotonNetwork.LeaveRoom();
        }
    }

    /// <summary>
    /// Kiểm tra xem nó có đang là master client hay không.
    /// </summary>
    public static bool IsMaster
    {
        get
        {
            if (AA_GameMng.IsTestMode)
            {
#if UNITY_EDITOR
                return true;
#endif
                return false;
            }
            return PhotonNetwork.isMasterClient;
        }
    }



    public void Send_MoveData(AA_MoveDataForTranfer moveData)
    {
        string sendData = JsonUtility.ToJson(moveData);
        if (!AA_GameMng.IsTestMode)
            photon.RPC("Received_MoveData_Net", PhotonTargets.Others, sendData);
        moveData.Reset();
    }

    [RPC]
    public void Received_MoveData_Net(string receiveData)
    {
        AA_MoveDataForTranfer moveData = JsonUtility.FromJson<AA_MoveDataForTranfer>(receiveData);
        AA_GameMng.Current.StartMyTurn(moveData);
    }


    /// <summary>
    /// Gửi thông tin thiết lập bàn cờ cho người bên kia
    /// </summary>
    public void Send_ChessBoardData(AA_BoardDataForTranfer boardData)
    {
        //Chỉ cần gửi dữ liệu sang cho bên kia thôi, vì dữ liệu bên này đã có rồi không cần đồng bộ
        string sendData = JsonUtility.ToJson(boardData);
        if (!AA_GameMng.IsTestMode)
            photon.RPC("Received_ChessBoardData_Net", PhotonTargets.Others, sendData);
    }

    /// <summary>
    /// Nhận
    /// </summary>
    [RPC]
    private void Received_ChessBoardData_Net(string receiveDataJon)
    {
        AA_GameMng.Current.IsOtherReady = true;
        AA_UISceneMain.Current.SetReadyOn(!AA_NetworkMng.IsMaster);

        //Lưu lại các thuộc tính này lại
        Debug.Log("Nhận được data: " + receiveDataJon);
        AA_GameMng.Current.OtherBoardData = JsonUtility.FromJson<AA_BoardDataForTranfer>(receiveDataJon);
        IsLoadOtherDataDone = true;

        if (AA_GameMng.Current.IsReady)
        {
            AA_GameMng.Current.StartTheGame();
        }
    }


    #endregion

    #region Network Events

    void OnConnectedToMaster()
    {
        Debug.Log("Ket noi ");
    }
    void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby: " + PhotonNetwork.lobby.Name);
        PhotonNetwork.JoinRandomRoom();
    }
    void OnLeftLobby()
    {
        Debug.Log("OnLeftLobby");
    }
    void OnJoinedRoom()
    {
        /*
        Gọi sự kiện Join ok:
        - Load main scene
        */
        if (OnJoinedOK != null)
            OnJoinedOK();
        Debug.Log("OnJoinedRoom: " + PhotonNetwork.room.name + " - " + PhotonNetwork.room.playerCount);
    }

    /// <summary>
    /// Sự kiện mà người chơi
    /// </summary>
    void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        AA_UISceneMain.Current.SendInfoToOther(newPlayer);
        if (OnOtherPlayerJoined != null)
            OnOtherPlayerJoined();
    }

    void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");
        if (OnLeftOK != null)
            OnLeftOK();
    }
    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("OnPhotonRandomJoinFailed");
        PhotonNetwork.CreateRoom(PhotonNetwork.lobby.Name + "/" + DateTime.Now.ToShortTimeString(), new RoomOptions() { maxPlayers = 2 }, null);
    }

    void OnPhotonCreateRoomFailed()
    {
        Debug.Log("OnPhotonCreateRoomFailed");
    }
    #endregion
}
