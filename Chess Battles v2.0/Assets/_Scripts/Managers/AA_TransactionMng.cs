﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class AA_TransactionMng : MonoBehaviour {
    public static AA_TransactionMng Instance;
    public Transform image_1;
    public Transform image_2;
    public Transform centerPoint;
    public Text title;

    public Transform pos1;
    public Transform pos2;

    public bool isTestMode;
    public bool isShowLog;

    void Awake()
    {
        AA_GameMng.IsTestMode = this.isTestMode;

        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
        DOTween.Init();
    }

    void Start()
    {

        title.gameObject.SetActive(false);
    }

    public void SetStatus(string status)
    {
        title.text = status;
    }

    public void Close()
    {
        image_1.DOMove(centerPoint.position, 1);
        image_2.DOMove(centerPoint.position, 1);
        title.gameObject.SetActive(true);
    }

    public void Open()
    {
        image_1.DOMove(pos1.position, 1);
        image_2.DOMove(pos2.position, 1);
        title.gameObject.SetActive(false);
    }
	
}
