﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class quản lý toàn bộ game logic
/// Quản lý các quân cờ trên bàn cờ
/// </summary>
public class AA_GameMng : MonoBehaviour
{
    public static AA_GameMng Current;
    public static bool IsTestMode = true;

    /// <summary>
    /// Bắt đầu game
    /// </summary>
    public event System.Action OnGameBegin;

    /// <summary>
    /// Sau khi kết thúc lượt đi của đối thủ sẽ đến lượt mình
    /// </summary>
    public event System.Action OnMyTurnStart;

    /// <summary>
    /// Sau khi kết thúc lượt đi của mình
    /// </summary>
    public event System.Action OnMyTurnEnd;

    public AA_ENGameState GameState { get; private set; }

    public bool IsCurrentReady { get; set; }
    public bool IsOtherReady { get; set; }

    /// <summary>
    /// Check xem cả 2 đã sẵn sàng hay chưa?
    /// </summary>
    public bool IsReady
    {
        get
        {
            if (AA_GameMng.IsTestMode)
                return true;
            else
                return IsOtherReady && IsCurrentReady;
        }
    }

    //Thông tin của bàn cờ bên địch, được set lại khi bên kia ready
    public AA_BoardDataForTranfer OtherBoardData { get; set; }

    public AA_GameMng()
    {
        Current = this;
        if (AA_NetworkMng.IsMaster)
            GameState = AA_ENGameState.IS_WAITING;
        else GameState = AA_ENGameState.NEW;
    }

    /// <summary>
    /// TRò chơi bắt đầu:
    /// 1. Sinh các quân cờ phía địch
    /// </summary>
    public void StartTheGame()
    {
        Debug.Log("START THE GAME!");
        if (!IsTestMode)
            AA_ChessBoard.Current.GenerateOtherBoardData(OtherBoardData);
        if (OnGameBegin != null)
            OnGameBegin();
    }

    /// <summary>
    /// Được gọi khi đi hết lượng dice ở lượt đi của mình:
    /// 1. Chuyển trạng thái
    /// 2. Gửi thông tin sang bên địch
    /// </summary>
    public void FinishMyTurn(AA_MoveDataForTranfer myMoveData)
    {
        if (IsTestMode == false)
        {
            //0. Chuyển trạng thái
            SetGameState(AA_ENGameState.INGAME_IS_MY_TURN, false);
            //1. Chuyển thông tin
            AA_NetworkMng.Instance.Send_MoveData(myMoveData);
        }

        // Gửi event sang cho các chỗ khác
        if (OnMyTurnEnd != null)
            OnMyTurnEnd();

        // Nếu đang ở trạng thái test thì chuyển luôn sang lượt mình
        if (IsTestMode)
            StartMyTurn(null);
    }

    /// <summary>
    /// Chưa hết dice nhưng chủ động click finish
    /// </summary>
    void Event_OnClickFinishMyTurn()
    {
        FinishMyTurn(AA_ChessBoard.Current.MovesRecord);

        if (IsTestMode)
            StartMyTurn(null);
    }

    /// <summary>
    /// Được gọi khi nhận được data từ bên địch gửi về
    /// 1. Nhận thông tin các nước đi của địch -> Thực hiện các nước đi đó
    /// 2. Chuyển trạng thái
    /// </summary>
    public void StartMyTurn(AA_MoveDataForTranfer otherMoveData)
    {
        Debug.Log("StartMyTurn");
        if (otherMoveData != null)
        {
            //0. Thực hiện lại nước đi của quân địch
            SetGameState(AA_ENGameState.IS_PLAYING_ANIMATION, true);
            AA_AnimationMng.Current.GenerateMove(AA_ChessBoard.Current, otherMoveData);
        }
        else
        {
            //1. Chuyển trạng thái
            ResetGameState(AA_ENGameState.INGAME_IS_MY_TURN);

            if (OnMyTurnStart != null)
                OnMyTurnStart();
        }
    }

    /// <summary>
    /// Sự kiện được gọi khi chạy xong animation
    /// </summary>
    void Event_OnAnimationDone()
    {
        //1. Chuyển trạng thái
        ResetGameState(AA_ENGameState.INGAME_IS_MY_TURN);

        if (OnMyTurnStart != null)
            OnMyTurnStart();
    }

    public void ResetGameState(AA_ENGameState state)
    {
        this.GameState = state;
    }

    /// <summary>
    /// Set 1 trạng thái game là on hay off
    /// </summary>
    public void SetGameState(AA_ENGameState state, bool isOn)
    {
        if (isOn)
        {
            //Set cho bit thứ k = 1
            this.GameState |= state;
        }
        else
        {
            //Set cho bit thứ k = 0
            this.GameState &= (~state);
        }
    }

    public bool IsGameState(AA_ENGameState state)
    {
        return (GameState & state) != 0;
    }

    #region Các events mà nó sẽ nhận
    void OnEnable()
    {
        AA_SelectNhanVat.Current.OnSelectedNhanVatDone += Event_OnSelectedDone;
        AA_UISceneMain.Current.OnClickFinish += Event_OnClickFinishMyTurn;
        AA_AnimationMng.Current.OnAnimationDone += Event_OnAnimationDone;
    }


    void OnDisable()
    {
        AA_SelectNhanVat.Current.OnSelectedNhanVatDone -= Event_OnSelectedDone;
        AA_UISceneMain.Current.OnClickFinish -= Event_OnClickFinishMyTurn;
        AA_AnimationMng.Current.OnAnimationDone -= Event_OnAnimationDone;
    }


    void Event_OnSelectedDone()
    {
        AA_ChessBoard.Current.SetAllCellState(AA_ENCellState.NORMAL);
        if (IsTestMode == true)
        {
            SetGameState(AA_ENGameState.INGAME_IS_MY_TURN, true);
        }
        else
        {
            //Nếu là máy chủ thì nó được đánh, còn không thì không được đánh
            if (AA_NetworkMng.IsMaster)
                SetGameState(AA_ENGameState.INGAME_IS_MY_TURN, true);
            else
                SetGameState(AA_ENGameState.INGAME_IS_MY_TURN, false);
        }
    }

    #endregion
}
