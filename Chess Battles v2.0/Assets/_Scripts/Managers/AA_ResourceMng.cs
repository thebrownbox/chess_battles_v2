﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Xây dựng một lớp resource cơ bản chỉ có phương thức chủ đạo là GET.
/// Quản lý việc chỉ Load ra resouce 1 lần duy nhất
/// </summary>
public class AA_ResourceMng
{
    private static AA_ResourceMng _instance = null;
    public static AA_ResourceMng Instance
    {
        get
        {
            if (_instance == null)
                _instance = new AA_ResourceMng();
            return _instance;
        }
    }
    private Dictionary<string, Object> _dict;
    private Dictionary<string, Object[]> _dict2;
    // Use this for initialization
    private AA_ResourceMng()
    {
        _dict = new Dictionary<string, Object>();
        _dict2 = new Dictionary<string, Object[]>();
    }

    /// <summary>
    /// Get 1 đối tượng thường
    /// </summary>
    public Object Get(string path)
    {
        if (_dict.ContainsKey(path) == false)
            _dict.Add(path, UnityEngine.Resources.Load(path));
        return _dict[path];
    }

    /// <summary>
    /// Lấy 1 tập các object
    /// </summary>
    public Object[] GetAll(string path)
    {
        if (_dict2.ContainsKey(path) == false)
            _dict2.Add(path, UnityEngine.Resources.LoadAll(path));
        return _dict2[path];
    }

}
