﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Quản lý các thông số, thông tin của bàn cờ
/// Quản lý các ô cờ Cells và các quân cờ Pieces
/// 
/// => Chỗ này chỉ nên làm chỗ lưu dữ liệu và tạo ra các hàm để thao tác với dữ liệu thôi.
/// Không nên thực hiện logic trong bàn cở ở class này, hãy thực hiện ở Game Manager.
/// Tương tự Game Manager cũng không nên tự tay trọc vào data của Chess Board mà Board sẽ public ra các function để
/// Manager sử dụng.
/// </summary>
public class AA_ChessBoard : MonoBehaviour
{
    public const int Dice_Max = 1;
    public const int Dice_Min = 6;

    public static AA_ChessBoard Current = null;
    public static AA_Location2D MAX, MIN;

    public GameObject cellPrefap;
    public Camera currentCamera;

    /// <summary>
    /// Giá trị dice hiện tại
    /// </summary>
    public int DiceValue { get; private set; }

    /// <summary>
    /// Tất cả các ô cờ
    /// </summary>
    private Dictionary<AA_Location2D, AA_BaseCell> _cells = new Dictionary<AA_Location2D, AA_BaseCell>();

    /// <summary>
    /// Tất cả các quân cờ hiện tại trên bàn cờ
    /// </summary>
    private List<AA_BasePiece> _pieces = new List<AA_BasePiece>();

    /// <summary>
    /// Ô cờ hiện tại đang được chọn
    /// </summary>
    private AA_BaseCell _currentSelectedCell { get; set; }

    /// <summary>
    /// Lưu các ô hiện tại đang ở trạng thái targeted
    /// </summary>
    private List<AA_BaseCell> _currentTargetedCells { get; set; }

    /// <summary>
    /// Thông tin của map
    /// </summary>
    private AA_MapData data;

    /// <summary>
    /// Lưu thông tin di chuyển của người chơi.
    /// Đây là thông tin truyền đi cho người chơi còn lại.
    /// </summary>
    public AA_MoveDataForTranfer MovesRecord { get; private set; }

    void Awake()
    {
        MovesRecord = new AA_MoveDataForTranfer();
        Current = this;
        data = AA_GameData.Instance.MapsData[AA_UserSelected.SelectedZone];
    }

    void Start()
    {
        //1. Sinh bàn cờ
        foreach (AA_CellInfo info in data.cells)
        {
            GameObject obj = Instantiate<GameObject>(cellPrefap);
            obj.transform.parent = this.transform;

            AA_BaseCell cell = obj.GetComponent<AA_BaseCell>();
            if (AddCell(info.loc, cell))
                cell.SetInfo(info);

        }
        Debug.Log("cells: " + data.cells.Count);

        //2. Chọn camera tương ứng
        if (AA_NetworkMng.IsMaster)
        {
            //Master
            currentCamera.transform.position = data.cam_1.pos;
            currentCamera.transform.rotation = data.cam_1.rot;
        }
        else
        {
            //Client
            currentCamera.transform.position = data.cam_2.pos;
            currentCamera.transform.rotation = data.cam_2.rot;
        }

        // Sinh đoạn quân đối thủ
        if(AA_GameMng.IsTestMode)
        {
            GenerateOtherBoardData(AA_Tools.GetTestAnemies());
        }

    }

    #region Các thao tác với Cells

    /// <summary>
    /// Gán tất cả các quân cờ vào cùng một trạng thái.
    /// TH1: Khi tất cả đã ready.
    /// </summary>
    public void SetAllCellState(AA_ENCellState state)
    {
        foreach (AA_BaseCell cell in _cells.Values)
            cell.SetCellState(state);
    }

    /// <summary>
    /// Hiển thị CÁC ô ở phía quân army, và đang không có quân nào đứng đó.
    /// Chuyển trạng thái nó sang targeted
    /// </summary>
    public void ShowSetupable(AA_ENArmyType army)
    {
        foreach (AA_BaseCell cell in _cells.Values)
        {
            if (cell.IsMyCell(AA_NetworkMng.IsMaster))
            {
                if (cell.currentPiece == null)
                    cell.SetCellState(AA_ENCellState.TARGETED);
                else
                    cell.SetCellState(AA_ENCellState.NORMAL);
            }
        }
    }

    /// <summary>
    /// Lấy 1 ô thuộc quân army đang trống
    /// </summary>
    public AA_BaseCell GetAvaiableCell(AA_ENArmyType army)
    {
        //Tùy vào loại quân mà thứ tự duyệt khác nhau
        foreach (AA_BaseCell cell in _cells.Values)
            if (cell.IsMyCell(AA_NetworkMng.IsMaster)
                && cell.currentPiece == null)
                return cell;
        return null;
    }

    /// <summary>
    /// Get the cell at location x,y
    /// </summary>
    public AA_BaseCell GetCell(int x, int y)
    {
        AA_Location2D location;
        location.X = x;
        location.Y = y;
        if (_cells.ContainsKey(location))
            return _cells[location];
        return null;
    }

    /// <summary>
    /// Get the cell at location
    /// </summary>
    public AA_BaseCell GetCell(AA_Location2D location)
    {
        if (_cells.ContainsKey(location))
            return _cells[location];
        return null;
    }

    /// <summary>
    /// Hàm này được gọi lúc ban đầu khi khởi tọa bàn cờ.
    /// Nó đưa cell về vị trí đúng của nó.
    /// Thêm nó vào _cells để quản lý.
    /// - Thêm đoạn tìm MAX MIN ở trong này
    /// </summary>
    public bool AddCell(AA_Location2D location, AA_BaseCell cell)
    {
        //Nếu đã tồn tại location đó thì 
        if (_cells.ContainsKey(location))
        {
            GameObject.Destroy(cell.gameObject);
            return false;
        }
        else
        {
            MAX.X = Mathf.Max(MAX.X, location.X);
            MAX.Y = Mathf.Max(MAX.Y, location.Y);
            MIN.X = Mathf.Min(MIN.X, location.X);
            MIN.Y = Mathf.Min(MIN.Y, location.Y);

            _cells.Add(location, cell);
            return true;
        }
    }

    /// <summary>
    /// Thực hiện chuyển hết các ô trạng thái khác normal về normal
    /// </summary>
    private void ClearStatedCells()
    {
        if (_currentSelectedCell != null)
            _currentSelectedCell.SetCellState(AA_ENCellState.NORMAL);

        if (_currentTargetedCells != null && _currentTargetedCells.Count > 0)
            foreach (var item in _currentTargetedCells)
                item.SetCellState(AA_ENCellState.NORMAL);
    }
    #endregion

    #region Các thao tác với Pieces
    /// <summary>
    /// Thêm vào danh sách để quản lý
    /// </summary>
    public void Add(AA_BasePiece piece)
    {
        if (_pieces.Contains(piece) == false)
            _pieces.Add(piece);
    }

    public void Remove(AA_BasePiece piece)
    {
        if (piece != null)
        {
            _pieces.Remove(piece);
            Destroy(piece.gameObject);
        }
    }

    /// <summary>
    /// Sinh các quân cờ của quân địch khi trò chơi bắt đầu.
    /// </summary>
    public void GenerateOtherBoardData(AA_BoardDataForTranfer otherBoard)
    {
        foreach (AA_PieceData pieceData in otherBoard.pieces)
        {
            string resoucePath = "Prefaps/" + pieceData.armyType + "_" + pieceData.pieceType;

            GameObject GO = Instantiate<GameObject>(AA_ResourceMng.Instance.Get(resoucePath) as GameObject);
            GO.transform.parent = this.transform;

            AA_BasePiece piece = GO.GetComponent<AA_BasePiece>();
            piece.SetData(pieceData, this);

            AA_ChessBoard.Current.Add(piece);
        }
    }


    /// <summary>
    /// Lấy thông tin của bàn cờ tại thời điểm hiện tại
    /// Thường dùng để lấy thông tin bàn cờ đầu tiên để gửi sang
    /// </summary>
    public AA_BoardDataForTranfer GetCurrentBoardData()
    {
        AA_BoardDataForTranfer boardData = new AA_BoardDataForTranfer();
        for (int i = 0; i < _pieces.Count; i++)
            boardData.pieces.Add(_pieces[i].GetData());
        return boardData;
    }
    #endregion


    #region Events
    void OnEnable()
    {
        AA_UserInput.Current.OnSelectedCell += Event_OnSelectedCell;
    }
    void OnDisable()
    {
        AA_UserInput.Current.OnSelectedCell -= Event_OnSelectedCell;
    }
   
    /// <summary>
   /// Sự kiện người dùng chọn được 1 ô
   /// Thực hiện các thao tác quân trọng.
   /// </summary>
    void Event_OnSelectedCell(AA_BaseCell newSelectedCell)
    {
        //chỉ xử lý nếu ddang là lượt của mình
        if (AA_GameMng.Current.IsGameState(AA_ENGameState.INGAME_IS_MY_TURN))
        {
            //0. Nếu chọn phải ô đang được selected => Di chuyển
            if (newSelectedCell.State == AA_ENCellState.TARGETED)
            {
                //0. Lưu lại nước đi
                AA_MoveData newMove = new AA_MoveData();
                newMove.from = _currentSelectedCell.GetLocation();
                newMove.to = newSelectedCell.GetLocation();
                MovesRecord.Add(newMove);

                int moveCount = _currentSelectedCell.currentPiece.MoveTo(this, newSelectedCell);

                //3. Để các ô thành trạng thái bình thường
                ClearStatedCells();

                //4. Trừ đi lượng dice
                DiceValue -= moveCount;

                AA_UISceneMain.SetStatus(DiceValue.ToString());

                if (DiceValue <= 0)
                {
                    AA_GameMng.Current.FinishMyTurn(MovesRecord);
                    AA_UISceneMain.Current.Click_FinishMyTurn();
                }
            }
            else
            {
                //Nếu chọn phải ô không được Targeted
                //=> Chỉ xử lý khi nó là quân ta, còn nếu là quân địch thì không làm gì
                if (newSelectedCell.currentPiece != null && newSelectedCell.currentPiece.IsSameSide(AA_NetworkMng.IsMaster))
                {
                    //1. Xóa những ô đang target và selected đi
                    ClearStatedCells();
                    _currentSelectedCell = newSelectedCell;

                    //2. Thực hiện show mới
                    _currentSelectedCell.SetCellState(AA_ENCellState.SELECTED);
                    _currentTargetedCells = newSelectedCell.currentPiece.GetMoveableCell(this, DiceValue);
                    foreach (AA_BaseCell cell in _currentTargetedCells)
                        cell.SetCellState(AA_ENCellState.TARGETED);
                }
            }
        }
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    public void SetDiceValue(int value)
    {
        this.DiceValue = value;
        AA_UISceneMain.SetStatus(DiceValue.ToString());
    }
}
