﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// Quản lý game flow trong game, chứ không quản lý game logic
/// </summary>
public class AA_SceneMain : MonoBehaviour
{
    void Start()
    {
        AA_TransactionMng.Instance.Open();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Click_Logout();
        }
    }
    void Instance_OnLeftOK()
    {
        AA_TransactionMng.Instance.Open();
        SceneManager.LoadScene(AA_MyPath.SceneSelectBattlefield);
    }

    void OnEnable()
    {
        AA_NetworkMng.Instance.OnLeftOK += Instance_OnLeftOK;

    }

    void OnDisable()
    {
        AA_NetworkMng.Instance.OnLeftOK -= Instance_OnLeftOK;

        AA_UserData.SaveData();
    }

   
    public void Click_Logout()
    {
        AA_TransactionMng.Instance.SetStatus("Leaving...");
        AA_TransactionMng.Instance.Close();
       if(AA_GameMng.IsTestMode)
       {
           Instance_OnLeftOK();
       }
       else
       {
           AA_NetworkMng.Instance.Logout();
       }
    }

}
