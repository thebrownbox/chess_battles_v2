﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class AA_SceneSelectArmyMng : MonoBehaviour
{

    #region UI events
    public void ClickSelect(int a)
    {
        AA_UserSelected.SelectedArmy = (AA_ENArmyType) a;
        SceneManager.LoadScene(AA_MyPath.SceneSelectBattlefield);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(AA_MyPath.SceneStart);
        }
    }
    #endregion
}
