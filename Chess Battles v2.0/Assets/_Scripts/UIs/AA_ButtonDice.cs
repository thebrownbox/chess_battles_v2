﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class AA_ButtonDice : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public static AA_ButtonDice Current;
    AA_ButtonDice() { Current = this; }

    public event Action OnDiceButtonUp;
    public event Action OnDiceButtonDown;

    public void OnPointerUp(PointerEventData eventData)
    {
        if (OnDiceButtonUp != null)
            OnDiceButtonUp();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OnDiceButtonDown != null)
            OnDiceButtonDown();
    }
}