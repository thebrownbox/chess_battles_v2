﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AA_UISceneMain : Photon.MonoBehaviour
{
    public event System.Action OnClickFinish;
    public Button btFinish;
    public int viewId = 998;
    public Text txt_Status;
    public static AA_UISceneMain Current;
    public Image masterAvatar;
    public Image clientAvatar;
    public Text masterName;
    public Text clientName;
    public Text masterReady;
    public Text clientReady;
    private PhotonView photon;
    public GameObject panelChonNhanVat;
    public Text txt_DiceValue;
    public GameObject xucXac;

    private int DiceValue { get; set; }
    /// <summary>
    /// Xúc xắc
    /// </summary>

    void Awake()
    {
        Current = this;
    }

    /// <summary>
    /// Thực hiện hiển thị status
    /// </summary>
    public static void SetStatus(string status)
    {
        Current.txt_Status.text = status;
    }

    void Start()
    {
        Input.simulateMouseWithTouches = true;
        SetStatus("Waiting for other player...");

        //Thực hiện load các thông tin người dùng lên
        //Khi đã vào trong này rồi thì tức là đã vào phòng ok
        photon = this.gameObject.AddComponent<PhotonView>();
        photon.viewID = viewId;
        LoadCurrentInfo();

        if (!AA_NetworkMng.IsMaster)
            SendInfoToOther(PhotonNetwork.masterClient);
    }


    public void LoadCurrentInfo()
    {
        if (AA_NetworkMng.IsMaster)
        {
            masterName.text = FB_UserInfo.Current.first_name;
            StartCoroutine(LoadImage(masterAvatar, FB_UserInfo.Current.id));
        }
        else
        {
            clientName.text = FB_UserInfo.Current.first_name;
            StartCoroutine(LoadImage(clientAvatar, FB_UserInfo.Current.id));
        }
    }

    /// <summary>
    /// Thực hiện load hình ảnh, thông tin của người chơi mới join vào phòng
    /// </summary>
    public void SendInfoToOther(PhotonPlayer otherPlayer)
    {
        if (AA_GameMng.IsTestMode == false)
        {
            photon.RPC("Receive_OhterInfo_Net", otherPlayer, FB_UserInfo.Current.first_name, FB_UserInfo.Current.id);
        }
    }

    [RPC]
    public void Receive_OhterInfo_Net(string name, string id)
    {
        if (!PhotonNetwork.isMasterClient)
        {
            masterName.text = name;
            StartCoroutine(LoadImage(masterAvatar, id));
        }
        else
        {
            clientName.text = name;
            StartCoroutine(LoadImage(clientAvatar, id));
        }
    }

    IEnumerator LoadImage(Image image, string id)
    {
        if (!AA_GameMng.IsTestMode)
        {
            string url = "https://graph.facebook.com/" + id + "/picture?type=normal";
            WWW www = new WWW(url);
            yield return www;
            image.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));
        }
    }

    public void SetReadyOn(bool isMaster)
    {
        if (isMaster)
            masterReady.gameObject.SetActive(true);
        else clientReady.gameObject.SetActive(true);
    }

    public void Click_CloseChonNhanVat()
    {
        panelChonNhanVat.SetActive(false);
    }

    /// <summary>
    /// Sự kiện người dùng chủ động kết thúc lượt chơi của mình
    /// </summary>
    public void Click_FinishMyTurn()
    {
        btFinish.gameObject.SetActive(false);
        xucXac.SetActive(false);

        AA_ChessBoard.Current.SetDiceValue(0);
        if (OnClickFinish != null)
            OnClickFinish();
    }

    #region Events
    void OnEnable()
    {
        AA_AnimationMng.Current.OnAnimationDone += Event_OnAnimationDone;
        AA_ButtonDice.Current.OnDiceButtonDown += Event_OnDiceButtonDown;
        AA_ButtonDice.Current.OnDiceButtonUp += Event_OnDiceButtonUp;
        AA_GameMng.Current.OnGameBegin += Event_OnGameStart;
        AA_GameMng.Current.OnMyTurnStart += Event_OnNewTurnStart;
        AA_GameMng.Current.OnMyTurnEnd += Current_OnMyTurnEnd;
    }

    void OnDisable()
    {
        AA_AnimationMng.Current.OnAnimationDone -= Event_OnAnimationDone;
        AA_ButtonDice.Current.OnDiceButtonDown -= Event_OnDiceButtonDown;
        AA_ButtonDice.Current.OnDiceButtonUp -= Event_OnDiceButtonUp;
        AA_GameMng.Current.OnGameBegin -= Event_OnGameStart;
        AA_GameMng.Current.OnMyTurnStart -= Event_OnNewTurnStart;
        AA_GameMng.Current.OnMyTurnEnd -= Current_OnMyTurnEnd;
    }

    void Event_OnGameStart()
    {
        if (AA_NetworkMng.IsMaster)
        {
            SetStatus("GAME START!");
            xucXac.SetActive(true);
            btFinish.gameObject.SetActive(true);
        }
    }

    void Event_OnNewTurnStart()
    {
        txt_DiceValue.text = "0";
        btFinish.gameObject.SetActive(true);
        xucXac.SetActive(true);
    }
    void Current_OnMyTurnEnd()
    {
        btFinish.gameObject.SetActive(false);
    }
    /// <summary>
    /// Sự kiện kết thúc animation di chuyển của các quân cờ.
    /// 1. Hiển thị nút tung xúc sắc 
    /// 2. Chuyển trạng thái sang trạng thái thực hiện animation
    /// </summary>
    void Event_OnAnimationDone()
    {
        AA_GameMng.Current.SetGameState(AA_ENGameState.IS_PLAYING_ANIMATION, false);
        AA_GameMng.Current.SetGameState(AA_ENGameState.IS_ROLLING_DICE, true);
        xucXac.SetActive(true);
    }

    private bool _isRolling = false;
    void Event_OnDiceButtonDown()
    {
        _isRolling = true;
        StartCoroutine(GennerateNumber());
    }

    void Event_OnDiceButtonUp()
    {
        xucXac.SetActive(false);
        AA_GameMng.Current.SetGameState(AA_ENGameState.IS_ROLLING_DICE, false);
        _isRolling = false;
        Debug.Log("Dice: " + DiceValue);
        AA_ChessBoard.Current.SetDiceValue(DiceValue);
    }

    IEnumerator GennerateNumber()
    {
        while (_isRolling)
        {
            DiceValue = Random.Range(AA_ChessBoard.Dice_Min, AA_ChessBoard.Dice_Max);
            txt_DiceValue.text = DiceValue.ToString();
            yield return new WaitForSeconds(0.05f);
        }
    }

    #endregion


    #region Các thao tác xúc xắc


    #endregion

}
