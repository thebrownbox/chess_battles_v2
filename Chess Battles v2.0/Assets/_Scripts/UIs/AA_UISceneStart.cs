﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class AA_UISceneStart : MonoBehaviour
{
    public static AA_UISceneStart Current;
    #region UI elements
    public Image img_Avatar;
    public Text txt_Name;
    public Button bt_Start;
    public Button bt_Login;
    #endregion

    void Awake()
    {
        //Xử lý màn hình nếu như là dạng PC
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            float W = 800;
            float H = 480;
            float k = W / H;
            float p = 0.5f;

            Resolution[] resolutions = Screen.resolutions;
            int width = (int)((resolutions[resolutions.Length - 1].width) * p);
            int height = (int)(width * H / W);
            Screen.SetResolution(width, height, false);
        }


        Current = this;

        //if(Application.platform != RuntimePlatform.WindowsEditor)
        //    bt_Start.gameObject.SetActive(false);
        if (AA_UserData.LoadData() && AA_UserData.Current.isLoggedIn)
        {
            bt_Start.gameObject.SetActive(true);
            bt_Login.gameObject.SetActive(false);
        }
        else
        {
            bt_Start.gameObject.SetActive(false);
            bt_Login.gameObject.SetActive(true);
        }

        if(Application.platform == RuntimePlatform.WindowsPlayer)
        {
            bt_Start.gameObject.SetActive(true);
            bt_Login.gameObject.SetActive(false);
        }
    }
    void Start()
    {
        Debug.Log("Check: " + Application.isMobilePlatform);
        AA_GameData.Instance.LoadData();
    }

    void OnEnable()
    {
        AA_FBHelper.Instance.OnLoggedIn += Instance_OnLoggedIn;
    }

    void OnDisable()
    {
        AA_FBHelper.Instance.OnLoggedIn -= Instance_OnLoggedIn;
    }

    void Instance_OnLoggedIn()
    {
        LoadAvatar();
        bt_Start.gameObject.SetActive(true);
    }


    #region Events
    public void LoadAvatar()
    {

    }
    #endregion

}
